import React from 'react';

const OrderCount = (props) => {
    return (
        <div>{(props.sum !== 0 ) ?
            <div>
                Total order price: {props.sum}
                {props.items.map((item) => (
                    <div>{item.name} : {Math.ceil(parseInt(item.price) + parseInt(item.price)/100 * parseInt(props.tip) + props.delivery/props.items.length)} KGS</div>
                ))}
            </div>: null}
        </div>
    );
};

export default OrderCount;