import React from 'react';
import './AddPerson.css';
const AddPerson = (props, i) => {
    return (
        <div className="addPerson-input">
            <input className="addPerson-name"
                   type="text"
                   value={props.name}
                   placeholder="Name"
                   onChange={props.changeName}
            />&nbsp;
            <input className="addPerson-price"
                   type="text"
                   value={props.price}
                   placeholder="Price"
                   onChange={props.changePrice}
            /> KGS &nbsp;
            <button className="addPerson-delete" onClick={() => props.removeItem(i)}><strong>X</strong></button>
        </div>
    );
};

export default AddPerson;