import React from 'react';
import './ExtraCharge.css';

const ExtraCharge = (props) => {
    return (
        <div>
            <div>
                <div className="extra-charge">Tips: </div>
                <input className="extra-charge-input"
                       type="text"
                       value={props.tip}
                       onChange={props.changeTip}/> %
            </div>
            <div>
                <div className="extra-charge">Delivery: </div>
                <input className="extra-charge-input"
                       type="text"
                       value={props.delivery}
                       onChange={props.changeDelivery}/> KGS
            </div>
        </div>
    );
};

export default ExtraCharge;