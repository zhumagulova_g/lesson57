import React, {useState} from 'react';
import './TotalOrder.css'

const TotalOrder = () => {
    const [totalOrder, setTotalOrder] = useState(
    {person: 0, sum: 0, tip: 0, delivery: 0, totalSum: 0});

    const onChangePerson = e => {
      const totalOrderCopy = {...totalOrder};
      totalOrderCopy.person = e.target.value;
      setTotalOrder(totalOrderCopy);
    };

    const onChangePrice = e => {
        const totalOrderCopy = {...totalOrder};
        totalOrderCopy.sum = e.target.value;
        setTotalOrder(totalOrderCopy);
    };

    const onChangeTip = e => {
        const totalOrderCopy = {...totalOrder};
        totalOrderCopy.tip = e.target.value;
        setTotalOrder(totalOrderCopy);
    };

    const onChangeDelivery = e => {
        const totalOrderCopy = {...totalOrder};
        totalOrderCopy.delivery = e.target.value;
        setTotalOrder(totalOrderCopy);
    };

    const countTotal = () => {
        const totalOrderCopy = {...totalOrder};
        totalOrderCopy.totalSum = 0;
        totalOrderCopy.totalSum = parseInt(totalOrderCopy.sum) + parseInt(totalOrderCopy.sum) / 100 * parseInt(totalOrderCopy.tip) + parseInt(totalOrderCopy.delivery);
        setTotalOrder(totalOrderCopy);
    };

    return (
        <div className="total-order">
            <div>
                <span className="total-class">Person:</span>
                <input className="total-input" type="text" value={totalOrder.person}
                        onChange={e => onChangePerson(e)}/>
            </div>
            <div>
                <span className="total-class">Total price: </span>
                <input className="total-input" type="text" value={totalOrder.sum}
                       onChange={e => onChangePrice(e)}/> KGS
            </div>
            <div>
                <span className="total-class">Tip: </span>
                <input className="total-input" type="text" value={totalOrder.tip}
                       onChange={e => onChangeTip(e)}/> %
            </div>
            <div>
                <span className="total-class">Delivery: </span>
                <input className="total-input" type="text" value={totalOrder.delivery}
                       onChange={e => onChangeDelivery(e)}/> KGS
            </div>
            <button className="total-btn" onClick={countTotal}><strong>Count</strong></button>
            <div>{(totalOrder.totalSum > 0) ? (<div>
                    <div>Total order price: {totalOrder.totalSum} KGS</div>
                    <div>Person: {totalOrder.person}</div>
                    <div>Price for each: {Math.ceil(totalOrder.totalSum / parseInt(totalOrder.person))} KGS</div>
                </div>) : null}
            </div>
        </div>
    );
};

export default TotalOrder;