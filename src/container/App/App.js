import React, {useState} from 'react';
import './App.css';
import Order from "../Order/Order";
import TotalOrder from "../TotalOrder/TotalOrder";

const App = () => {
    const [mode, setMode] = useState('first');

  const onRadioChange = e => {
    setMode(e.target.value);
  }

  return (
    <div className="App">
        <div className="radio">
            <p>
                <input type="radio"
                       name="options"
                       value="first"
                       onChange={onRadioChange}
                       checked={mode==='first'}
                /> First option
            </p>
            <p>
                <input type="radio"
                       name="options"
                       value="second"
                       onChange={onRadioChange}
                       checked={mode==='second'}
                /> Second option
            </p>
        </div>
        <p className="result">
            {mode ==='first' && (
                <Order/>
            )}
            {mode ==='second' && (
                <TotalOrder/>
            )}
        </p>
    </div>
  );
}

export default App;
