import React, {useState} from 'react';
import AddPerson from "../../component/AddPerson/AddPerson";
import ExtraCharge from "../../component/ExtraCharge/ExtraCharge";
import OrderCount from "../../component/OrderCount/OrderCount";

const Order = () => {
    const [items, setItems] = useState([{name: '', price: 0}]);

    const [total, setTotal] = useState({sum: 0, tip: 0, delivery: 0});

    const addItem = () => {
        setItems([...items, {name: '', price: 0}]);
    }

    const onChangeTip = (e) => {
        const totalCopy = {...total};
        totalCopy.tip = e.target.value;
        setTotal(totalCopy);
    };

    const onChangeDelivery = (e) => {
        const totalCopy = {...total};
        totalCopy.delivery = e.target.value;
        setTotal(totalCopy);
    };

    const onChangeName = (e, i) => {
        const itemsCopy = [...items];
        const itemCopy = {...itemsCopy[i]};
        itemCopy.name = e.target.value;
        itemsCopy[i] = itemCopy;
        setItems(itemsCopy);
    }

    const onChangePrice = (e, i) => {
        const itemsCopy = [...items];
        const itemCopy = {...itemsCopy[i]};
        itemCopy.price = e.target.value;
        itemsCopy[i] = itemCopy;
        setItems(itemsCopy);
    }

    const removeItem = (i) => {
        const itemsCopy = [...items];
        itemsCopy.splice(i, 1);
        setItems(itemsCopy);
    };

    const countOrders = () => {
        const totalCopy = {...total};
        totalCopy.sum = 0;
        const itemsCopy = [...items];
        itemsCopy.map((item) => {
            return totalCopy.sum += parseInt(item.price);
        })
        totalCopy.sum += Math.ceil(totalCopy.sum / 100 * parseInt(totalCopy.tip) + parseInt(totalCopy.delivery));
        console.log(totalCopy.sum);
        setTotal(totalCopy);
        setItems(itemsCopy);
    };

    return (
        <div>{items.map((item, i) => (
            <AddPerson
                key={i}
                name={item.name}
                price={item.price}
                changeName={(e) => onChangeName(e, i)}
                changePrice={(e) => onChangePrice(e, i)}
                removeItem={removeItem}
            />
             ))}

            <button onClick={addItem}><strong>Add</strong></button>

            <ExtraCharge
                tip={total.tip}
                delivery={total.delivery}
                changeTip={e => onChangeTip(e)}
                changeDelivery={e => onChangeDelivery(e)}
            />

            <button onClick={countOrders}><strong>Count</strong></button>

            <OrderCount
                sum={total.sum}
                tip={total.tip}
                delivery={total.delivery}
                items={items}
            />
        </div>
    );
};

export default Order;